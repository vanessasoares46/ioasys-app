import { combineReducers } from 'redux'
import authEmpresas from './authEmpresas'
import dataEmpresas from './dataEmpresas'


export default combineReducers({
    authEmpresas,
    dataEmpresas
})


