export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERRO = 'LOGIN_ERRO'

const initialState = {
  client: "",
  'access-token': "",
  uid: ""
}

export default function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST: {
      return {
        ...state
      };
    }
    case LOGIN_SUCESS: {
      return {
        ...state,
        client: action.payload.headers['client'],
        'access-token': action.payload.headers['access-token'],
        uid: action.payload.headers['uid']
      };
    }
    case LOGIN_ERRO: {
      return {
        loading: false,
        isAuthenticated: false,
        error: action.error
      };
    }

    default:
      return state;
  }
}