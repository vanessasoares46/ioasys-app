
export const EMPRESAS_SUCCESS = 'EMPRESAS_SUCCESS';

const initialState = {
  empresas: [],
};

export default function empresas(state = initialState, action) {
  switch (action.type) {
    case EMPRESAS_SUCCESS: {
      return {
        ...state,
        empresas: action.payload,
      };
    }

    default:
      return state;
  }
}

