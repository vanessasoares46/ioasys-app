export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS =  'LOGIN_SUCCESS';
export const LOGIN_ERRO ='LOGIN_ERRO';

export const loginRequest = (payload) => {
    return {
        type: LOGIN_REQUEST,
        payload
    } 
}
export const loginSuccess = (payload) => {
  return {
    type: LOGIN_SUCCESS,
    payload
  }
};
export const loginErro = (erro) => {
    return {
      type: LOGIN_ERRO,
      erro
    }
  };
