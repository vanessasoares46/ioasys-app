
import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import LoginPage from './LoginPage'
import Home from './Home';
import VisualizarEmpresas from './components/VisualizarEmpresas'

class App extends Component {
  
  render() {
    return (  
    <Router>
          <Switch>
              <Route exact path='/' component={LoginPage} />      
              <Route patch = '/home' component = {Home}/> 
              <Route patch = '/details' component = {VisualizarEmpresas}/> 
          </Switch>
      </Router>
    );
  }
}

export default App;