import React from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { LOGIN_REQUEST, loginSuccess, login_Erro } from './actions/authEmpresasAction'
import { withRouter } from 'react-router-dom'


const API_URL = 'https://empresas.ioasys.com.br/api/v1'

class App extends React.Component {
  constructor() {
    super();
    this.state = { loading: false, email: '', senha: '' };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClick(state) {
    this.setState({ loading: true });
    axios.post(`${API_URL}/users/auth/sign_in`, {
      email: state.state.email,
      password: state.state.password
    }).then((resp) => {
      this.props.dispatch(loginSuccess(resp));
      this.props.history.push('/home');
      this.setState({ loading: false });
    }).catch(resp => {
      this.setState({ loading: false });
      console.log(resp);
    });
  }


  render() {
    return (
      <div>
        {this.state.loading && <div className='loading-container'>
          <div className='rotate paddingTop50'>
            <FontAwesomeIcon id="loading" icon="circle-notch" className="loading" />
          </div>
        </div>}
        <div className={'container paddingtop ' + (this.state.loading ? 'blur' : '')}>
          <div className='row pt-5'>
            <div className='col-12 d-flex justify-content-center'>
              <img src="./logo-home.png" srcSet="./logo-home@2x.png 2x, ./logo-home@3x.png 3x" />
            </div>
          </div>
          <div className='row pt-5'>
            <div className='col-12 d-flex justify-content-center'>
              <p className='BEM-VINDO-AO-EMPRESA'> BEM-VINDO AO EMPRESAS</p>
            </div>
          </div>
          <div className='row pt-5'>
            <div className='col-12 d-flex justify-content-center'>
              <p className='Lorem-ipsum-dolor-si'> Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            </div>
          </div>
          <div className='row pt-5'>
            <div className='col-12 d-flex justify-content-center'>
              <div className="input-group mb-3 email-pass">
                <div className="input-group-prepend">
                  <img src="./ic-email.svg" className="ic_email" />
                </div>
                <input type="text" name='email' value={this.state.email} onChange={(event) => this.handleChange(event)} className="text-line" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-12 d-flex justify-content-center'>
              <div className="input-group mb-3 email-pass">
                <div className="input-group-prepend">
                  <img src="./ic-cadeado.svg" className="ic_cadeado" />
                </div>
                <input type="password" name='password' value={this.state.password} onChange={(event) => this.handleChange(event)} className="text-line" placeholder="Senha" aria-label="Username" aria-describedby="basic-addon1" />
              </div>
            </div>
          </div>
          <div className='row pt-5'>
            <div className='col-12 d-flex justify-content-center'>
              <button onClick={() => this.handleClick(this)} type="submit" className="Rectangle-26"><span className='ENTER'>Entrar</span></button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { data: state.alunos };
}


export default withRouter(connect(mapStateToProps)(App))