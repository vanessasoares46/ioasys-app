import React, { Component } from 'react';
import AppBar from './components/AppBar';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import axios from 'axios';
//import { connect } from 'react-redux';
import './App.css'
class Home extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <AppBar></AppBar>
                <ul class="list-group">
                    {this.props.data.map((el) => {
                        return (
                            <li class=" base list-group-item" onClick={() => this.props.history.push('/empresa/' + el.id)}>
                                <div className='row pt-3'>
                                    <div className='col-2 Mask'>
                                    </div>
                                    <div className='col-10'>
                                        <div className='row Empresa1' style={{ padding: '20px' }} >{el.enterprise_name}</div>
                                        <div className='row Negcio' style={{ padding: '20px' }} >{el.city}</div>
                                        <div className='row' style={{ paddingLeft: '20px' }}>{el.country}</div>
                                    </div>
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return { data: state.dataEmpresas.empresas };
}

export default connect(mapStateToProps)(Home)