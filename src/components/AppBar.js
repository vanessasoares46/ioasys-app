import React, { Component } from 'react';
import { Link } from 'react-router'
import '../App.css';
import { connect } from 'react-redux';
import axios from 'axios';
import { empresasSuccess } from '../actions/dataEmpresasActions'

const API_URL = 'https://empresas.ioasys.com.br/api/v1'


class AppBar extends Component {

    constructor() {
        super();
        this.state = {
            change: true
        }

        this.toggleSearch = this.toggleSearch.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
    }

    toggleSearch() {
        this.setState({ change: !this.state.change });
        this.props.searched();
    }

    componentDidMount() {
        this.onKeyUp();
    }

    onKeyUp() {
        const props = this.props;

        axios.get(`${API_URL}/enterprises`, {
            headers: {
                client: props['client'],
                'access-token': props['access-token'],
                'uid': props['uid']
            }
        }).then((response) => {
            this.props.dispatch(empresasSuccess(response.data.enterprises));
        })
    }

    render() {
        return (
            <div className=" container-fluid navbar navbar-expand-lg navbar-light bg-light Rectangle-413 background">
                {this.state.change &&
                    <div className='row d-flex underline'>
                        <img src="./ic-search-copy.svg" class="ic_search-copy" />
                        <input type='text' className='text-line-search' style={{ flexGrow: 1 }} placeholder='Pesquisar' onKeyUp={this.onKeyUp}></input>
                        <img onClick={this.toggleSearch} src="./ic-close.svg" class="ic_close"></img>
                    </div>}
                {!this.state.change && <div className='row d-flex justify-content-around'>

                    <img src="./logo-nav.png" srcset="./logo-nav@2x.png 2x, ./logo-nav@3x.png 3x" style={{ textAlign: 'center' }} class="logo_nav" />
                    <img onClick={this.toggleSearch} src="./ic-search-copy.svg" style={{ flexGrow: 1 }} class="ic_search-copy" />

                </div>}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {

    return {
        data: state.dataEmpresas.empresas,
        client: state.authEmpresas.client,
        'access-token': state.authEmpresas['access-token'],
        uid: state.authEmpresas.uid
    };
}

export default connect(mapStateToProps)(AppBar);